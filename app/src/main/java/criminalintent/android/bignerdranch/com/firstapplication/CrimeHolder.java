package criminalintent.android.bignerdranch.com.firstapplication;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

/**
 * Created by sahil on 20/11/15.
 */
public class CrimeHolder extends RecyclerView.ViewHolder {

    protected TextView mThumbnail;
    protected TextView mTitle;


    public CrimeHolder(View itemView) {
        super(itemView);
        this.mThumbnail = (TextView) itemView.findViewById(R.id.text);
        this.mTitle = (TextView)itemView.findViewById(R.id.text2);

    }

}
