package criminalintent.android.bignerdranch.com.firstapplication;

import android.support.v4.app.Fragment;
import android.util.Log;

/**
 * Created by sahil on 19/11/15.
 */
public class FragmentOneActivity extends FirstActivity{
    @Override
    public Fragment createFragment() {
        Log.d("log", "Fragment started Successfully");
        return new FirstFragment();
    }
}
