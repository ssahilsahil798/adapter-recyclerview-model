package criminalintent.android.bignerdranch.com.firstapplication;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

/**
 * Created by sahil on 20/11/15.
 */
public class CrimeAdapter extends RecyclerView.Adapter<CrimeHolder> {
    public List<SampleData> feedList;
    private Context mContext;

    public CrimeAdapter(List<SampleData> feedList) {
        this.feedList = feedList;

    }

    @Override
    public CrimeHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.feed, null);
        CrimeHolder ch = new CrimeHolder(v);
        return ch;
    }

    @Override
    public void onBindViewHolder(CrimeHolder holder, int i) {
        SampleData sd = feedList.get(i);
        holder.mThumbnail.setText(sd.getmThumbnail());
        holder.mTitle.setText(sd.getmTitle());

    }



    @Override
    public int getItemCount() {
        return feedList.size();
    }
}
