package criminalintent.android.bignerdranch.com.firstapplication;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;

import android.os.Bundle;

import android.support.v4.app.FragmentManager;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;
import java.util.zip.Inflater;

/**
 * Created by sahil on 19/11/15.
 */
public class FirstFragment extends Fragment {

    private RecyclerView mRecyclerView;
    private CrimeAdapter mAdapter;
    private List<SampleData> feedList;
    private SampleData mSampleData;
    private Context mContext;




    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_1, container, false);

        feedList = new ArrayList<SampleData>();
        for(int i=0;i<200;i++){
            SampleData sd = new SampleData();
            sd.setmThumbnail("the thumbnail here" + i);
            sd.setmTitle("the title here" + i);
            feedList.add(sd);
        }
        Log.d("sd", String.valueOf(feedList.size()));
        mRecyclerView = (RecyclerView) v.findViewById(R.id.recycler_view);
        mRecyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 2, GridLayoutManager.VERTICAL, false));
        mAdapter = new CrimeAdapter(feedList);
        mRecyclerView.setAdapter(mAdapter);

        return v;
    }
}
